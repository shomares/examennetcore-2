import { Component, OnInit, Input } from '@angular/core';
import { IProducts } from '../model/Products-model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  constructor() { }


  @Input()
  item : IProducts;

  ngOnInit(): void {
  }

}
