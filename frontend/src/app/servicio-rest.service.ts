import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ILogin} from 'src/app/model/Login-model';
import { Observable } from 'rxjs';
import { IProducts } from './model/Products-model';
import {pluck} from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServicioRestService {

  private URL : string;
  constructor(private http: HttpClient) { 
    this.URL  = environment.localAPI;
  }

  login(user : ILogin) : Observable<any>{
    return this.http.post(`${this.URL}/api/Customer/authorize`, user);
  }

  getProducts (): Observable<IProducts[]>{
    return this.http.get<any>(`${this.URL}/api/Product`).pipe(
      pluck<any,IProducts[]> ('value')
    );
  }

}
