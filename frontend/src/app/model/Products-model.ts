export interface IProducts{
    ProductId : number;
    Name: string;
    ProductNumber : string;
    ThumbnailPhotoFileName : string;
    ThumbNailPhoto : any;
}