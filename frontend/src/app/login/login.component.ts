import { Component, OnInit } from '@angular/core';
import { ServicioRestService } from '../servicio-rest.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ILogin } from '../model/Login-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private service : ServicioRestService, private builder : FormBuilder, private router: Router ) { }

  formControl : FormGroup;

  ngOnInit(): void {
     this.formControl = this.builder.group({
        Username : [''],
        Password : ['']
     });
  }


  submit() : void{
    const value : ILogin = this.formControl.value;
     this.service.login(value).subscribe( s=> {
       console.log(s);
      this.router.navigate(['/items']);
     }, error => {
       alert('Usurio no autorizado')
     })
  }
  

}
