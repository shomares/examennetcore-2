import { Component, OnInit } from '@angular/core';
import { IProducts } from '../model/Products-model';
import { Subject, Subscription } from 'rxjs';
import { ServicioRestService } from '../servicio-rest.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  items : Subject<IProducts[]>;

  subscription : Subscription

  constructor(private service : ServicioRestService) { }

  ngOnInit(): void {
    this.items = new Subject<IProducts[]>();
    this.subscription = this.service.getProducts().subscribe(this.items); 
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
