﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using examennetcore_2.Bussines;
using examennetcore_2.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace examennetcore_2.Controllers
{
    [ApiController]
    [Route("api/Customer")]
    public class WeatherForecastController : ControllerBase
    {
       
        private readonly ICustomerBussines bussines;

        public WeatherForecastController(ICustomerBussines bussines)
        {
            this.bussines= bussines;
        }

        /// <summary>
        /// Valida si el usuario tiene acceso a la API
        /// </summary>
        /// <response code="200">EL usuario tiene acceso a la api</response>
        /// <response code="401">El usuario no tiene acceso a la api</response>     
        [HttpPost("authorize")]
        [AllowAnonymous]   
        public async Task<IActionResult>  Authenticate(MUsuario user)
        {
            var res = await this.bussines.Login(user.Username, user.Password);

            if(res!=null)
            {
                return this.Ok(res);
            }
            else
            {
                return this.Unauthorized();
            }
        }

       /// <summary>
        /// Registra a los usuarios
        /// </summary>
        /// <response code="200">Lista de usuarios</response>
        /// <response code="404">No hay usuarios registrados</response>     
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]   
    
        public async Task<IActionResult> Registrar(CustomerDto usuario)
        {
            var res = await this.bussines.Register(usuario);

            if(res!=null)
            {
                return this.Ok();
            }else
            {
                return this.NotFound();
            }
        }
    }
}
