namespace examennetcore_2.Controllers {
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNet.OData;
    using Microsoft.AspNet.OData.Routing;
    using examennetcore_2.Context;
  
    /// <summary>
    /// Contactos query controller
    /// </summary>
    [ODataRoutePrefix("Product")]
    public class ProductoQueryController : ODataController {

        /// <summary>
        /// Base de datos de contactos
        /// </summary>
        private readonly ExamenContext context;

        /// <summary>
        /// Crea una instancia de QueryController para OData
        /// </summary>
        /// <param name="context">Source de datos</param>
        public ProductoQueryController( ExamenContext context)
        {
            this.context = context;
        }

        [EnableQuery]
        [ODataRoute]
        public IActionResult Get() => this.Ok(this.context.Product);

        [EnableQuery]
        [ODataRoute("({id})")]
        public IActionResult Get(int id)
        {
            var query = this.context.Product.Where( s=> s.ProductId == id);

            if (query != null)
            {
                return this.Ok(query);
            }
            else{
                return this.NotFound();
            }
        }

    }
}