using System;
namespace examennetcore_2.Transactional{
    public interface ITransactional: IDisposable 
    {
        void Commit();

        void Rollback();

        void Begin();

        
    }
}
