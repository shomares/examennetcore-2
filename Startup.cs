
namespace examennetcore_2
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Context;
using Microsoft.EntityFrameworkCore;
using examennetcore_2.Bussines;
using examennetcore_2.Bussines.Impl;
using examennetcore_2.Transactional;
using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.IdentityModel.Tokens;
    using System.Text;
    using Microsoft.AspNet.OData.Extensions;
    using Microsoft.OData.Edm;
    using Microsoft.AspNet.OData.Builder;
    using examennetcore_2.Models;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ExamenContext>(optionsBuilder => optionsBuilder.UseSqlServer("Server=tcp:examenmabs.database.windows.net,1433;Initial Catalog=examenmadb20;Persist Security Info=False;User ID=modernappssadmin;Password=T<8v3/H$;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
            services.AddSingleton<IHash>(s => new Hash(this.Configuration.GetValue<string>("secret")));
            services.AddTransient<ITransactional>(s => s.GetService<ExamenContext>());
            
            services.AddTransient<ICustomerBussines>(s => 
                new CustomerBussinesTransactional(new CustomerBussines(
                                        s.GetService<ExamenContext>(),
                                        s.GetService<IHash>()
                                        ), s.GetService<ITransactional>())
            );

            
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                 builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
            }));

            services.AddOData ();
            services.AddControllers ();
            services.AddMvc (options => {
                options.EnableEndpointRouting = false;

            });
            this.ConfigureAuth(services);
        }

        private void ConfigureAuth(IServiceCollection services)
        {
            var key = this.Configuration.GetValue<string>("secret");
         
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

        }

        private IEdmModel GetEdmModel () {
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder ();
            builder.EntitySet<Products> ("Product");
            builder.EntitySet<Customer> ("Customer");
            return builder.GetEdmModel ();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors("MyPolicy");
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            
              app.UseMvc (p => {
                p.EnableDependencyInjection ();
                p.Select ().Expand ().Count ().Filter ().OrderBy ().MaxTop (100).SkipToken ().Build ();
                p.MapODataServiceRoute ("api", "api", this.GetEdmModel ());

                p.MapRoute (
                    name: "default",
                    template: "{controller}");

            });



          
        }
    }
}
