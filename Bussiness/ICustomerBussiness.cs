namespace examennetcore_2.Bussines{
    using System.Threading.Tasks;
    using Dto;
    public interface ICustomerBussines{
        Task<CustomerDto> Register(CustomerDto data);
        Task<CustomerDto> Login (string user, string password);
    }
}