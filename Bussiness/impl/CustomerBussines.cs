namespace examennetcore_2.Bussines
{
    using System.Threading.Tasks;
    using Dto;
    using Context;
    using Microsoft.EntityFrameworkCore;
    using examennetcore_2.Models;
    using System;
    
    public class CustomerBussines : ICustomerBussines
    {
        private readonly ExamenContext context;

        private readonly IHash hash;

        public CustomerBussines(ExamenContext context, IHash hash){
            this.context = context;
            this.hash = hash;
        }

       
        public async Task<CustomerDto> Login(string user, string password)
        {
            var userDb = await this.context.Customer
                        .FirstOrDefaultAsync(s=> s.EmailAddress == user );
            string passwordDB = string.Empty;

            if (userDb != null)
            {
                passwordDB = this.hash.HashPassword(password, userDb.PasswordSalt);

                if(passwordDB == userDb.PasswordHash){
                    var customer =  new CustomerDto{
                        CustomerId =  userDb.CustomerId,
                        LastName = userDb.LastName
                    };

                    var token = this.hash.GetToken(customer);
                    customer.Token = token;
                    return customer;
                }
            }

            return null;
        }

        public async Task<CustomerDto> Register(CustomerDto data)
        {
            string passwordSalt = Guid.NewGuid().ToString().Substring(0, 3);
            
            var password = this.hash.HashPassword(data.Password,passwordSalt );

            var user = new Customer{
                CompanyName = data.CompanyName,
                EmailAddress = data.EmailAddress,
                FirstName = data.FirstName,
                LastName = data.LastName,
                MiddleName = data.MiddleName,
                ModifiedDate = new System.DateTime(),
                NameStyle = data.NameStyle,
                Phone = data.Phone,
                SalesPerson = data.SalesPerson,
                Suffix = data.Suffix,
                Title = data.Title,
                PasswordHash = password,
                PasswordSalt = passwordSalt
            };

            await this.context.AddAsync(user);
            await this.context.SaveChangesAsync();
            return data;
        }
    }
}