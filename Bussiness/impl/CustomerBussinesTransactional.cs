namespace examennetcore_2.Bussines
{
    using System.Threading.Tasks;
    using Dto;
    using System;
    using examennetcore_2.Transactional;

    public class CustomerBussinesTransactional : ICustomerBussines
    {
        private ITransactional transactional;
        private readonly ICustomerBussines bussines;


        public CustomerBussinesTransactional( ICustomerBussines bussines, ITransactional transactional){
           this.bussines = bussines;
           this.transactional = transactional;
        }

       
        public async Task<CustomerDto> Login(string user, string password)
        {
            CustomerDto dto = null;
           try{
               this.transactional.Begin();
               dto = await this.bussines.Login(user, password);
               this.transactional.Commit();
               return dto;

           }catch(Exception){
               this.transactional.Rollback();
               throw;
           }
        }

        public async Task<CustomerDto> Register(CustomerDto data)
        {
            CustomerDto dto = null;
           try{
               this.transactional.Begin();
               dto = await this.bussines.Register(data);
               this.transactional.Commit();
               return dto;

           }catch(Exception){
               this.transactional.Rollback();
               throw;
           }
        }
    }
}