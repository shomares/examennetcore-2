namespace examennetcore_2.Bussines{
    using System.Threading.Tasks;
    using Dto;
    public interface IHash{
        
        string HashPassword(string password, string salt);

        string GetToken(CustomerDto usuario);
    }
}